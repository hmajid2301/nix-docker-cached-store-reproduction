{
  pkgs ? import ./nix/pkgs.nix
}:

pkgs.stdenv.mkDerivation (rec {
  name = "env";

  buildInputs = with pkgs; [
    pkgs.path # nixpkgs path, not dependency, add nixpkgs source to gc-root and prevent it to be gc collected

    gnumake
    git
    nix
    arion
    # yarn2nix.yarn2nix
    pnpm
    docker
    docker-compose
    docker-volume-rm-if-exists

    # (nodejs_8_x.override { enableNpm = false; }) # not working on v10 https://github.com/svanderburg/node2nix/issues/79

    nodejs
    # yarn

    # for building c++ extensions (from https://matrix.ai/2018/03/24/developing-with-nix/)
    nodePackages.node-gyp
    nodePackages.node-gyp-build
    nodePackages.node-pre-gyp
  ];

  NIX_PATH = pkgs.lib.concatStringsSep ":" [
    "nixpkgs=${pkgs.path}"
  ];

  HISTFILE = toString ../.bash_hist;

  shellHook = ''
    export PATH="$PWD/node_modules/.bin/:$PATH"

    echo "shell hook yo"
  '';

  # if [[ -d node_modules || -L node_modules ]]; then
  #   echo "./node_modules is present. Replacing."
  #   rm -rf node_modules
  # fi

  # ln -s "${nodeModules}/node_modules" node_modules
})
