# fix error
test -f /etc/nsswitch.conf || echo '"hosts": "files dns"' > /etc/nsswitch.conf

# set default store path to store in cached folder
# echo "store = $(pwd)/.mycache" > /etc/nix/nix.conf

mkdir -p $(pwd)/.mycache/nix/store

# make same permissions as in
# stat /nix/store
chown 0:30000 $(pwd)/.mycache/nix/store
chmod 1775 $(pwd)/.mycache/nix/store
