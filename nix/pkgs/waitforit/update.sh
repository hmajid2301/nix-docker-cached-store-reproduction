#!/usr/bin/env nix-shell
#!nix-shell -i bash -p nix-prefetch-git

SCRIPT_DIR=$(dirname "$(readlink -f "$BASH_SOURCE")")
version="v1.3.1"
url="https://github.com/maxcnunes/waitforit/releases/download/$version/waitforit-linux_amd64"

sha256=$(nix-prefetch-url --type sha256 $url)

cat > $SCRIPT_DIR/revision.json <<EOL
{
  "version": "$version",
  "sha256": "$sha256",
  "url": "$url"
}
EOL
