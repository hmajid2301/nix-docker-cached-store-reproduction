pkgs: pkgsOld:
{
  gitCryptUnlock             = pkgs.callPackage ./gitCryptUnlock {};
  makeDockerComposeFile      = pkgs.callPackage ./makeDockerComposeFile {};
  opensslDecrypt             = pkgs.callPackage ./opensslDecrypt {};
  writeShellScript           = pkgs.callPackage ./writeShellScript {};
  arion                      = pkgs.callPackage ./arion {};
  gitignore                  = pkgs.callPackage ./nix-gitignore {};
  readRevision               = pkgs.callPackage ./readRevision {};
  yarn2nix                   = pkgs.callPackage ./yarn2nix {};
  waitforit                  = pkgs.callPackage ./waitforit {};
  wait-for-postgres          = pkgs.callPackage ./wait-for-postgres {};
  minio-set-bucket-policies  = pkgs.callPackage ./minio-set-bucket-policies {};
  docker-volume-rm-if-exists = pkgs.callPackage ./docker-volume-rm-if-exists {};
  db-tests-prepare           = pkgs.callPackage ./db-tests-prepare {};
  pgtest                     = pkgs.callPackage ./pgtest {};
  shmig                      = pkgs.callPackage ./shmig { withPSQL = true; };
  pnpm2nix                   = pkgs.callPackage ./pnpm2nix {};
  pnpm                       = pkgs.callPackage ./pnpm {};
}
