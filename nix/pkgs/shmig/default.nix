{ stdenv, fetchFromGitHub
, withMySQL ? false, withPSQL ? false, withSQLite ? false
, mysql, postgresql, sqlite, gawk, which, gnugrep, findutils, gnused
, lib, readRevision
}:

# copyed from
# https://github.com/NixOS/nixpkgs/blob/92a047a6c4d46a222e9c323ea85882d0a7a13af8/pkgs/development/tools/database/shmig/default.nix

let
  revData = readRevision ./revision.json;
in

stdenv.mkDerivation {
  name = "shmig";

  src = fetchFromGitHub {
    inherit (revData) rev sha256 owner repo;
  };

  makeFlags = [ "PREFIX=$(out)" ];

  postPatch = ''
    patchShebangs .

    substituteInPlace shmig \
      --replace "\`which mysql\`" "${lib.optionalString withMySQL "${mysql.client}/bin/mysql"}" \
      --replace "\`which psql\`" "${lib.optionalString withPSQL "${postgresql}/bin/psql"}" \
      --replace "\`which sqlite3\`" "${lib.optionalString withSQLite "${sqlite}/bin/sqlite3"}" \
      --replace "awk" "${gawk}/bin/awk" \
      --replace "which" "${which}/bin/which" \
      --replace "grep" "${gnugrep}/bin/grep" \
      --replace "find" "${findutils}/bin/find" \
      --replace "sed" "${gnused}/bin/sed"
  '';

  preBuild = ''
    mkdir -p $out/bin
  '';

  meta = with stdenv.lib; {
    description = "Minimalistic database migration tool with MySQL, PostgreSQL and SQLite support";
    homepage = "https://github.com/mbucc/shmig";
    license = licenses.bsd3;
    maintainers = with maintainers; [ ma27 ];
  };
}
