{ pkgs, fetchFromGitHub, readRevision, ... }:

let
  src = fetchFromGitHub (
    readRevision ./revision.json
  );

  arion = pkgs.callPackage "${src}/arion.nix" {};
in
  arion
